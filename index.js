// alert(`Hi`);

let number = 10;
let getCube = number ** 3;

console.log(`The cube of ${number} is ${getCube}. `)


//---------------------------

let address = [`258`, `Washington Ave NW`, `California`, `90011`];

let [buildingNumber, avenue, state, zipCode] = address;
console.log(`I live at ${buildingNumber} ${avenue}, ${state} ${zipCode}`);

//---------------------------

let animal = {
	name: `Lolong`,
	type: `saltwater crocodile`,
	weight: `1075 kgs`,
	measurement: `20 ft 3 in`
}

let {name, type, weight, measurement} = animal
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`)


//---------------------------

let arrayOfNum = [1, 2, 3, 4, 5];

arrayOfNum.forEach((num) => console.log(num));


//---------------------------


class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

} 

let myDog = new Dog(`Chase`, 4, `German Shepherd`);
console.log(myDog);






	